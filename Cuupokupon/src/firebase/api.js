import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { Constants } from '../common/Constants';
import { Languages } from '../common/Languages';


export function updateProduct(product, updateComplete) {
    product.updatedAt = firestore.FieldValue.serverTimestamp();

    firestore()
        .collection(Constants.COLLECTION.PRODUCT)
        .doc(product.id).set(product)
        .then(() => updateComplete(product))
        .catch((error) => console.log(error));
}

export function updateRestaurant(restaurant, updateComplete) {
    restaurant.updatedAt = firestore.FieldValue.serverTimestamp();

    firestore()
        .collection(Constants.COLLECTION.RESTAURANT)
        .doc(restaurant.id).set(restaurant)
        .then(() => updateComplete(restaurant))
        .catch((error) => console.log(error));
}

export function deleteProduct(product, deleteComplete) {

    firestore()
        .collection(Constants.COLLECTION.PRODUCT)
        .doc(product.id).delete()
        .then(() => deleteComplete())
        .catch((error) => console.log(error));
}



export async function getRestaurants(restaurantsRetrieved) {

    var restaurants = [];

    var snapshot = await firestore()
        .collection(Constants.COLLECTION.RESTAURANT)
        // .orderBy('createdAt')
        .get()

    snapshot.forEach((doc) => {
        const restaurantItem = doc.data();
        restaurantItem.id = doc.id;
        restaurants.push(restaurantItem);
    });

    restaurantsRetrieved(restaurants);
}

export async function getProducts(productsRetrieved) {
    var products = [];

    var snapshot = await firestore()
        .collection(Constants.COLLECTION.PRODUCT)
        .orderBy('CreatedAt')
        .get()

    snapshot.forEach((doc) => {
        const productItem = doc.data();
        productItem.id = doc.id;
        products.push(productItem);
    });

    productsRetrieved(products);
}

export function uploadPhoto(imageUri, callbackPercent, callbackUrl) {

    const fileName = String(imageUri).split(/[\\/]/).pop();
    var storageRef = storage().ref(`images/${fileName}`);

    storageRef.putFile(imageUri)
        .on(
            storage.TaskEvent.STATE_CHANGED,
            snapshot => {
                let percent = 0
                if (snapshot.totalBytes !== 0) {
                    percent = (snapshot.bytesTransferred / snapshot.totalBytes)
                }

                callbackPercent(percent)
            },
            error => {
                console.log("image upload error: " + error.toString());
            },
            () => {
                storageRef.getDownloadURL()
                    .then((downloadUrl) => {
                        callbackUrl(downloadUrl)
                    })
            }
        )

}

export function addRestaurant(restaurant, addComplete) {
    firestore()
        .collection(Constants.COLLECTION.RESTAURANT)
        .add(restaurant)
        .then((snapshot) => {
            restaurant.id = snapshot.id;
            snapshot.set(restaurant);
        }).then(() => addComplete(restaurant))
        .catch((error) => console.log(error));
}


export function addProduct(product, addComplete) {
    firestore()
        .collection(Constants.COLLECTION.PRODUCT)
        .add(product)
        .then((snapshot) => {
            product.id = snapshot.id;
            snapshot.set(product);
        }).then(() => addComplete(product))
        .catch((error) => console.log(error));
}