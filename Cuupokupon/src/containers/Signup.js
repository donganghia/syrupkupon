import React from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { Configs, SCREEN_WIDTH, PADDING_BOTTOM } from '../common/Configs';
import FastImage from 'react-native-fast-image';
import { Languages } from '../common/Languages';
import { Styles, ButtonStyles, InputStyles } from '../common/Styles';
import Button from '../components/Button';
import SimpleTextInput from '../components/SimpleTextInput';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import ScrollViewWithKeyboard from '../components/ScrollViewWithKeyboard';
import Validate from '../utils/Validate';
import auth from '@react-native-firebase/auth';
import Popup from '../components/Popup';
import MyLoading from '../components/MyLoading';

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            rePassword: "",
            error: {},
            isLoading: false
        }
    }

    validateData = () => {
        let error = {}
        const { email, password, rePassword } = this.state
        if (Validate.isStringEmpty(email)) {
            error['email'] = Languages.errorMessage.requireFieldEmail
        } else if (!Validate.isEmail(email)) {
            error['email'] = Languages.errorMessage.invalidEmail
        }

        if (Validate.isStringEmpty(password)) {
            error['password'] = Languages.errorMessage.requireFieldPass
        } else if (!Validate.validatePassWord(password)) {
            error['password'] = Languages.errorMessage.invalidPassword
        }

        if (Validate.isStringEmpty(rePassword)) {
            error['rePassword'] = Languages.errorMessage.requireFieldPass
        }

        if (password !== rePassword) {
            error['rePassword'] = Languages.errorMessage.notMatchPass
        }

        if (Object.keys(error).length > 0) {
            this.setState({ error })
        } else {
            this.setState({ error: {}, isLoading: true })
            this.onSignup(email, password)
        }
    }


    onSignup = (email, password) => {
        let objData = {
            "UserName": email,
            "Password": password
        }
        auth()
            .createUserWithEmailAndPassword(email, password)
            .then((data) => {
                this.setState({ isLoading: false })
                Navigator.replaceScreen(this, ScreenName.login, { dataLogin: objData })
            })
            .catch(error => {
                this.setState({ isLoading: false })
                if (error.code === 'auth/email-already-in-use') {
                    this.popupConfirm.openPopup()
                }
            });
    }

    onConfirm = () => {
        console.log('ok')
    }

    render() {
        const { error } = this.state
        return (
            <View style={styles.container}>
                <HeaderBar
                    navigation={this.props.navigation}
                    hasBack
                />
                <ScrollViewWithKeyboard contentContainerStyle={{ paddingBottom: PADDING_BOTTOM + 20 }}>
                    <View style={{ flex: 1 }}>
                        <View style={styles.wrapImg}>
                            <FastImage
                                style={styles.img}
                                source={require('../assets/images/bg_login.png')}
                                resizeMode="cover" />
                        </View>
                        <View style={{ flex: 3 }}>
                            <Text style={styles.txtDes}>{Languages.login.desStore}</Text>
                            <SimpleTextInput
                                containerStyle={styles.input}
                                styleType={InputStyles.white}
                                style={styles.textInput}
                                placeholder={Languages.login.email}
                                placeholderTextColor={Colors.txtGrayLight}
                                maxLength={100}
                                errorMsg={error['email']}
                                onChangeText={(email) => this.setState({ email })}
                            />

                            <SimpleTextInput
                                containerStyle={styles.input}
                                styleType={InputStyles.white}
                                style={styles.textInput}
                                placeholder={Languages.login.pass}
                                placeholderTextColor={Colors.txtGrayLight}
                                maxLength={20}
                                errorMsg={error['password']}
                                isPassword={true}
                                onChangeText={(password) => this.setState({ password })}
                            />

                            <SimpleTextInput
                                containerStyle={styles.input}
                                styleType={InputStyles.white}
                                style={styles.textInput}
                                placeholder={Languages.login.rePass}
                                placeholderTextColor={Colors.txtGrayLight}
                                maxLength={20}
                                errorMsg={error['rePassword']}
                                isPassword={true}
                                onChangeText={(rePassword) => this.setState({ rePassword })}
                            />

                            <Button
                                title={Languages.login.register}
                                containerStyle={styles.btnLogin}
                                style={ButtonStyles.pinkIcon}
                                onPress={this.validateData}
                            />

                            <Popup
                                ref={ref => this.popupConfirm = ref}
                                mainBtnText={Languages.common.ok}
                                content={Languages.errorMessage.existEmail}
                                onMainBtnPress={this.onConfirm}
                            />

                        </View>
                        {this.state.isLoading ? <MyLoading /> : null}
                    </View>
                </ScrollViewWithKeyboard>
            </View >
        )
    }
}



export default Signup;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    wrapImg: {
        alignItems: 'center', marginTop: 10
    },
    img: {
        height: SCREEN_WIDTH / 1.7,
        width: SCREEN_WIDTH / 1.7,
    },
    txtDes: {
        marginTop: 20,
        ...Styles.black18Bold,
        textAlign: 'center'
    },
    btnLogin: {
        marginTop: 20,
        marginHorizontal: 38
    },
    signup: {
        marginTop: 25,
        ...Styles.grayDark16weight300,
        textAlign: "center",
        textDecorationLine: 'underline'
    },

    input: {
        marginHorizontal: 38,
        marginTop: 10,
    },
    textInput: {
        ...Styles.grayLight14weight300,
        color: Colors.txtBlack,
        paddingHorizontal: 24,
    },
})
