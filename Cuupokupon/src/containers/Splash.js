import React from 'react';
import { StyleSheet, View } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import FastImage from 'react-native-fast-image';
import { SCREEN_WIDTH } from '../common/Configs';
import SessionInfo from '../models/SessionInfo';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import Validate from '../utils/Validate';


class Splash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.initData()
        }, 2000);
    }

    initData = () => {
        SessionInfo.initData(async () => {
            this.checkLogin()
        })
    }

    checkLogin = () => {
        if (!Validate.isObjectEmpty(SessionInfo.userInfo)) {
            Navigator.replaceScreen(this, ScreenName.restaurant)
        }
        else {
            Navigator.replaceScreen(this, ScreenName.login)
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <HeaderBar
                    noHeader
                />
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <FastImage
                        style={styles.img}
                        source={require('../assets/images/bg_login.png')}
                        resizeMode="cover" />
                </View>

            </View>
        )
    }
}



export default Splash;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    img: {
        height: SCREEN_WIDTH / 1.7,
        width: SCREEN_WIDTH / 1.7,
    }

})
