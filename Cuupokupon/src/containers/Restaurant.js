import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { SCREEN_WIDTH, PADDING_BOTTOM } from '../common/Configs';
import FastImage from 'react-native-fast-image';
import { Languages } from '../common/Languages';
import { Styles, ButtonStyles, } from '../common/Styles';
import Button from '../components/Button';
import SimpleTextInput from '../components/SimpleTextInput';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import ScrollViewWithKeyboard from '../components/ScrollViewWithKeyboard';
import ASSelectImageSrc from '../components/ASSelectImageSrc';
import Validate from '../utils/Validate';
import Utils from '../utils/Utils';
import { uploadPhoto, addRestaurant, getRestaurants, updateRestaurant } from '../firebase/api';
import * as Progress from 'react-native-progress';
import MyLoading from '../components/MyLoading';
import SessionInfo from '../models/SessionInfo';
import auth from '@react-native-firebase/auth';
import Touchable from '../components/Touchable';
import Icon from '../components/Icon';

class Restaurant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shopName: "",
            uriPhoto: "",
            uriIcon: "",
            address: "",
            urlMenu: "",
            error: {},
            isLoading: false,
            progressPhoto: 0,
            progressIcon: 0,
            imagePhoto: "",
            imageIcon: "",
            id: ""
        }
        this.userInfo = SessionInfo.userInfo
    }

    componentDidMount() {
        this.initData()
    }

    initData = () => {
        this.setState({ isLoading: true })
        getRestaurants(this.restaurantsRetrieved)
    }

    restaurantsRetrieved = (restaurants) => {
        this.setState({ isLoading: false })
        let foundIndex = restaurants.findIndex(val => val.UserId === this.userInfo.uid)
        if (foundIndex >= 0) {
            this.setState({
                imagePhoto: restaurants[foundIndex].UrlImage,
                imageIcon: restaurants[foundIndex].UrlIcon,
                address: restaurants[foundIndex].Address,
                shopName: restaurants[foundIndex].ShopName,
                urlMenu: restaurants[foundIndex].URLShop,
                id: restaurants[foundIndex].id
            })
        }

    }

    validateData = () => {
        let error = {}
        const { shopName, address, uriPhoto, uriIcon, imagePhoto, imageIcon } = this.state
        if (Validate.isStringEmpty(shopName)) {
            error['shopName'] = Languages.errorMessage.requireFieldShopName
        }

        if (Validate.isStringEmpty(address)) {
            error['address'] = Languages.errorMessage.requireFieldAddress
        }

        if (Validate.isStringEmpty(uriPhoto) && Validate.isStringEmpty(imagePhoto)) {
            error['photo'] = Languages.errorMessage.requireFieldShopPhoto
        }

        if (Validate.isStringEmpty(uriIcon) && Validate.isStringEmpty(imageIcon)) {
            error['icon'] = Languages.errorMessage.requireFieldShopIcon
        }

        if (Object.keys(error).length > 0) {
            this.setState({ error })
        } else {
            this.setState({ error: {}, isLoading: true })
            this.addRestaurant()
        }
    }

    addRestaurant = () => {
        this.setState({
            isLoading: true,
        })
        const params = {
            UrlImage: this.state.imagePhoto,
            ShopName: this.state.shopName,
            UrlIcon: this.state.imageIcon,
            Address: this.state.address,
            URLShop: this.state.urlMenu,
            CreatedAt: new Date(),
            UserId: SessionInfo.userInfo.uid
        }
        if (!Validate.isStringEmpty(this.state.id)) {
            params.id = this.state.id
            updateRestaurant(params, this.addComplete)
        }
        else {
            addRestaurant(params, this.addComplete)
        }


    }

    addComplete = (addComplete) => {
        this.setState({
            isLoading: false,
        }, () => Navigator.pushScreen(this, ScreenName.coupon))

    }

    onCancel = () => {
        auth().signOut()
        SessionInfo.logout()
        Navigator.replaceScreen(this, ScreenName.login)
    }

    uploadPhoto = () => {
        if (this.refs.srcSelect) {
            this.refs.srcSelect.show(1, 'photo')
        }
    }

    uploadIcon = () => {
        if (this.refs.srcSelect) {
            this.refs.srcSelect.show(1, 'icon')
        }
    }

    onImageSelected = (imgSL, key) => {
        if (Validate.isObjectEmpty(imgSL)) {
            return
        }
        if (key == 'photo') {
            this.setState({
                uriPhoto: Utils.getDisplayableImagePath(imgSL[0])
            })
            uploadPhoto(imgSL[0].path, this.returnPercentPhoto, this.returnImageUrlPhoto)
        } else if (key == 'icon') {
            this.setState({
                uriIcon: Utils.getDisplayableImagePath(imgSL[0])
            })
            uploadPhoto(imgSL[0].path, this.returnPercentIcon, this.returnImageUrlIcon)
        }



    }

    returnPercentPhoto = (percent) => {
        this.setState({
            progressPhoto: percent
        })
    }

    returnPercentIcon = (percent) => {
        this.setState({
            progressIcon: percent
        })
    }

    returnImageUrlPhoto = (imageUrl) => {
        this.setState({
            imagePhoto: imageUrl
        })
    }

    returnImageUrlIcon = (imageUrl) => {
        this.setState({
            imageIcon: imageUrl
        })
    }

    removeImage = () => {
        this.setState({
            uriPhoto: "",
            imagePhoto: ""
        })
    }

    removeIcon = () => {
        this.setState({
            uriIcon: "",
            imageIcon: ""
        })
    }

    render() {
        const { error } = this.state
        return (
            <View style={styles.container}>
                <HeaderBar
                    title={Languages.restaurant.restaurantInfo}
                />
                <ScrollViewWithKeyboard contentContainerStyle={{ paddingBottom: PADDING_BOTTOM + 30 }}>
                    <View style={styles.wrapInputInfo}>

                        <View style={[styles.input, { marginTop: 0 }]}>
                            <Text style={Styles.gray14weight500}>{Languages.restaurant.shopPhoto}</Text>

                            {
                                this.state.uriPhoto ?
                                    <View>
                                        <FastImage
                                            style={styles.imgPhoto}
                                            source={{ uri: this.state.uriPhoto }}
                                            resizeMode="cover" />
                                        <IconRemove onPress={this.removeImage} />
                                        {this.state.progressPhoto == 1 ? null : <Progress.Bar
                                            width={null}
                                            height={4}
                                            borderWidth={0}
                                            color={"red"}
                                            unfilledColor={"blue"}
                                            progress={this.state.progressPhoto}
                                            useNativeDriver={true}
                                        />}

                                    </View> : this.state.imagePhoto && this.state.id ?
                                        <View><FastImage
                                            style={styles.imgPhoto}
                                            source={{ uri: this.state.imagePhoto }}
                                            resizeMode="cover" />
                                            <IconRemove onPress={this.removeImage} />
                                        </View> :
                                        <View>
                                            <Touchable
                                                style={styles.wrapUploadImage}
                                                onPress={this.uploadPhoto}>
                                                <FastImage
                                                    style={styles.imageUpload}
                                                    source={require('../assets/images/ic_upload_image.png')}
                                                    resizeMode="cover" />

                                            </Touchable>
                                            {
                                                !Validate.isStringEmpty(error['photo']) ?
                                                    <Text style={styles.errText}>{error['photo']}</Text>
                                                    : null
                                            }
                                        </View>
                            }


                        </View>

                        <SimpleTextInput
                            title={Languages.restaurant.shopName}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={100}
                            value={this.state.shopName}
                            errorMsg={error['shopName']}
                            onChangeText={(shopName) => this.setState({ shopName })}
                        />

                        <View style={styles.input}>
                            <Text style={Styles.gray14weight500}>{Languages.restaurant.shopIcon}</Text>
                            {
                                this.state.uriIcon ? <View>
                                    <FastImage
                                        style={styles.imgPhoto}
                                        source={{ uri: this.state.uriIcon }}
                                        resizeMode="cover" />
                                    <IconRemove onPress={this.removeIcon} />
                                    {this.state.progressIcon == 1 ? null : <Progress.Bar
                                        width={null}
                                        height={4}
                                        borderWidth={0}
                                        color={"red"}
                                        unfilledColor={"blue"}
                                        progress={this.state.progressIcon}
                                        useNativeDriver={true}
                                    />}

                                </View> :
                                    this.state.imageIcon && this.state.id ? <View><FastImage
                                        style={styles.imgPhoto}
                                        source={{ uri: this.state.imageIcon }}
                                        resizeMode="cover" />
                                        <IconRemove onPress={this.removeIcon} />
                                    </View> :
                                        <View>
                                            <Touchable
                                                style={styles.wrapUploadImage}
                                                onPress={this.uploadIcon}>
                                                <FastImage
                                                    style={styles.imageUpload}
                                                    source={require('../assets/images/ic_upload_image.png')}
                                                    resizeMode="cover" />

                                            </Touchable>
                                            {
                                                !Validate.isStringEmpty(error['icon']) ?
                                                    <Text style={styles.errText}>{error['icon']}</Text>
                                                    : null
                                            }
                                        </View>
                            }
                        </View>

                        <SimpleTextInput
                            title={Languages.restaurant.address}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={100}
                            value={this.state.address}
                            errorMsg={error['address']}
                            onChangeText={(address) => this.setState({ address })}
                        />

                        <SimpleTextInput
                            title={Languages.restaurant.urlMenu}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={100}
                            value={this.state.urlMenu}
                            errorMsg={error['urlMenu']}
                            onChangeText={(urlMenu) => this.setState({ urlMenu })}
                        />

                    </View>
                    <View style={styles.wrapButton}>
                        <Button
                            title={Languages.restaurant.complete}
                            containerStyle={[styles.btn, { marginTop: 30 }]}
                            style={ButtonStyles.pink}
                            onPress={this.validateData}
                        />
                        <Button
                            title={Languages.restaurant.cancel}
                            containerStyle={styles.btn}
                            style={ButtonStyles.grayBorder}
                            onPress={this.onCancel}
                        />
                    </View>
                    <ASSelectImageSrc
                        ref={'srcSelect'}
                        onImageSelected={this.onImageSelected}
                        maxSelect={1}
                    />
                    {this.state.isLoading ? <MyLoading /> : null}


                </ScrollViewWithKeyboard>
            </View>
        )
    }
}

const IconRemove = ({ onPress }) => {
    return (
        <Touchable style={styles.closeContainer} onPress={onPress}
        >
            <Icon
                icon={require('../assets/images/ic_close.png')}
                size={12}
            />
        </Touchable>
    )
}



export default Restaurant;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    btn: {
        marginHorizontal: 38,
        marginTop: 20
    },
    wrapInputInfo: {
        paddingVertical: 30,
        backgroundColor: Colors.white, borderRadius: 15, justifyContent: 'center'
    },
    wrapButton: {
    },
    input: {
        marginHorizontal: 38,
        marginTop: 15,
    },
    textInput: {
        ...Styles.grayLight14weight300,
        color: Colors.txtBlack,
        paddingHorizontal: 24,
    },
    wrapUploadImage: {
        marginTop: 10,
        borderWidth: 1,
        borderStyle: 'dashed',
        backgroundColor: Colors.pinkLight,
        borderColor: Colors.pink,
        paddingVertical: 10,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: "center",
    },
    imageUpload: {
        width: 30,
        height: 26
    },
    imgPhoto: {
        width: SCREEN_WIDTH - 76,
        height: SCREEN_WIDTH / 2,
        borderRadius: 15,
        marginTop: 10
    },
    imgIcon: {

    },
    errText: {
        ...Styles.redError10weight500,
        textAlign: 'right',
        marginTop: 3,
        marginRight: 5
    },
    closeContainer: {
        ...Styles.shadow,
        position: 'absolute',
        backgroundColor: Colors.white,
        top: 0,
        right: -5,
        borderRadius: 50,
        padding: 10,
    },
})
