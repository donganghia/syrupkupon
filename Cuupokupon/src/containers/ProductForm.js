import React from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { Configs, SCREEN_WIDTH, PADDING_BOTTOM } from '../common/Configs';
import FastImage from 'react-native-fast-image';
import { Languages } from '../common/Languages';
import { Styles, ButtonStyles, InputStyles } from '../common/Styles';
import Button from '../components/Button';
import SimpleTextInput from '../components/SimpleTextInput';
import Navigator from '../routers/Navigator';
import Touchable from '../components/Touchable';
import ScrollViewWithKeyboard from '../components/ScrollViewWithKeyboard';
import Popup from '../components/Popup';
import Validate from '../utils/Validate';
import Utils from '../utils/Utils';
import { uploadPhoto, addProduct } from '../firebase/api';
import ASSelectImageSrc from '../components/ASSelectImageSrc';
import * as Progress from 'react-native-progress';
import MyLoading from '../components/MyLoading';
import SessionInfo from '../models/SessionInfo';
import Icon from '../components/Icon';

class ProductForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            couponContent: "",
            productName: "",
            newPrice: "",
            error: {},
            uriPhoto: "",
            imagePhoto: "",
            progressPhoto: 0,
            isLoading: false
        }
    }

    componentDidMount() {
        this.initData()
    }

    initData = () => {
        this.setState({
            couponCode: Utils.makeCouponCode(6)
        })
    }

    validateData = () => {
        let error = {}
        const { couponContent, productName, uriPhoto, newPrice, imagePhoto } = this.state
        if (Validate.isStringEmpty(couponContent)) {
            error['couponContent'] = Languages.errorMessage.requireFieldCouponContent
        }

        if (Validate.isStringEmpty(productName)) {
            error['productName'] = Languages.errorMessage.requireFieldProductName
        }

        if (Validate.isStringEmpty(uriPhoto)) {
            error['photo'] = Languages.errorMessage.requireFieldProductImage
        }
        else if (Validate.isStringEmpty(imagePhoto)) {
            error['photo'] = Languages.errorMessage.uploading
        }

        if (Validate.isStringEmpty(newPrice)) {
            error['newPrice'] = Languages.errorMessage.requireFieldProductPrice
        }

        if (Object.keys(error).length > 0) {
            this.setState({ error })
        } else {
            this.setState({ error: {}, isLoading: true })
            this.addProduct()
        }
    }

    addProduct = () => {
        this.setState({
            isLoading: true
        })
        const params = {
            UrlImage: this.state.imagePhoto,
            ProductName: this.state.productName,
            CouponContent: this.state.couponContent,
            NewPrice: this.state.newPrice,
            CreatedAt: new Date(),
            UserId: SessionInfo.userInfo.uid
        }
        addProduct(params, this.addComplete)
    }

    addComplete = (addComplete) => {
        this.setState({
            isLoading: false
        }, () => this.popupConfirm.openPopup())
    }


    uploadPhoto = () => {
        if (this.refs.srcSelect) {
            this.refs.srcSelect.show(1)
        }
    }

    onImageSelected = (imgSL) => {
        if (Validate.isObjectEmpty(imgSL)) {
            return
        }

        this.setState({
            uriPhoto: Utils.getDisplayableImagePath(imgSL[0])
        })
        uploadPhoto(imgSL[0].path, this.returnPercentPhoto, this.returnImageUrlPhoto)

    }

    returnPercentPhoto = (percent) => {
        this.setState({
            progressPhoto: percent
        })
    }

    returnImageUrlPhoto = (imageUrl) => {
        this.setState({
            imagePhoto: imageUrl
        })
    }

    onCancel = () => {
        Navigator.goBack(this)
    }

    removeImage = () => {
        this.setState({
            uriPhoto: "",
            imagePhoto: ""
        })
    }

    render() {
        const { error } = this.state
        return (
            <View style={styles.container}>
                <HeaderBar
                    navigation={this.props.navigation}
                    hasBack
                    title={Languages.productForm.title}
                />
                <ScrollViewWithKeyboard contentContainerStyle={{ paddingBottom: PADDING_BOTTOM }}>

                    <View style={styles.wrapInputInfo}>
                        <View style={[styles.input, { marginTop: 0 }]}>
                            <Text style={Styles.gray14weight500}>{Languages.productForm.productPhotos}</Text>

                            {
                                this.state.uriPhoto ?
                                    <View>
                                        <FastImage
                                            style={styles.imgPhoto}
                                            source={{ uri: this.state.uriPhoto }}
                                            resizeMode="cover" />
                                        <IconRemove onPress={this.removeImage} />
                                        {this.state.progressPhoto == 1 ? null : <Progress.Bar
                                            width={null}
                                            height={4}
                                            borderWidth={0}
                                            color={"red"}
                                            unfilledColor={"blue"}
                                            progress={this.state.progressPhoto}
                                            useNativeDriver={true}
                                        />}
                                        {
                                            !Validate.isStringEmpty(error['photo']) ?
                                                <Text style={styles.errText}>{error['photo']}</Text>
                                                : null
                                        }

                                    </View> :
                                    <View>
                                        <Touchable
                                            style={styles.wrapUploadImage}
                                            onPress={this.uploadPhoto}>
                                            <FastImage
                                                style={styles.imageUpload}
                                                source={require('../assets/images/ic_upload_image.png')}
                                                resizeMode="cover" />

                                        </Touchable>
                                        {
                                            !Validate.isStringEmpty(error['photo']) ?
                                                <Text style={styles.errText}>{error['photo']}</Text>
                                                : null
                                        }
                                    </View>
                            }


                        </View>

                        <SimpleTextInput
                            title={Languages.productForm.productName}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={100}
                            errorMsg={error['productName']}
                            onChangeText={(productName) => this.setState({ productName })}
                        />

                        <SimpleTextInput
                            title={Languages.productForm.couponContent}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={100}
                            errorMsg={error['couponContent']}
                            onChangeText={(couponContent) => this.setState({ couponContent })}
                        />

                        {/* <SimpleTextInput
                            title={Languages.productForm.couponCode}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={6}
                            value={this.state.couponCode.toUpperCase()}
                            errorMsg={error['couponCode']}
                            onChangeText={(couponCode) => this.setState({ couponCode })}
                        /> */}

                        <View style={{ marginHorizontal: 38, marginTop: 15 }}>
                            <Text style={Styles.gray14weight500}>{Languages.productForm.changeAmount}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <SimpleTextInput
                                    containerStyle={{ flex: 1, marginLeft: 10 }}
                                    style={styles.textInput}
                                    maxLength={100}
                                    errorMsg={error['newPrice']}
                                    keyboardType="numeric"
                                    value={this.state.newPrice}
                                    onChangeText={(newPrice) => this.setState({ newPrice })}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={styles.wrapButton}>
                        <Button
                            title={Languages.productForm.post}
                            containerStyle={[styles.btn, { marginTop: 30 }]}
                            style={ButtonStyles.pink}
                            onPress={this.validateData}
                        />
                        <Button
                            title={Languages.productForm.cancel}
                            containerStyle={[styles.btn, { marginBottom: 30 }]}
                            style={ButtonStyles.grayBorder}
                            onPress={this.onCancel}
                        />
                    </View>
                    <Popup
                        ref={ref => this.popupConfirm = ref}
                        mainBtnText={Languages.common.ok}
                        content={Languages.productForm.success}
                        onMainBtnPress={this.onCancel}
                    />
                    <ASSelectImageSrc
                        ref={'srcSelect'}
                        onImageSelected={this.onImageSelected}
                        maxSelect={1}
                    />
                    {this.state.isLoading ? <MyLoading /> : null}
                </ScrollViewWithKeyboard>
            </View>
        )
    }
}

const IconRemove = ({ onPress }) => {
    return (
        <Touchable style={styles.closeContainer} onPress={onPress}
        >
            <Icon
                icon={require('../assets/images/ic_close.png')}
                size={12}
            />
        </Touchable>
    )
}

export default ProductForm;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    btn: {
        marginHorizontal: 38,
        marginTop: 20
    },
    wrapInputInfo: {
        paddingVertical: 30,
        backgroundColor: Colors.white, borderRadius: 15, justifyContent: 'center'
    },
    wrapButton: {
    },
    input: {
        marginHorizontal: 38,
        marginTop: 15,
    },
    textInput: {
        ...Styles.grayLight14weight300,
        color: Colors.txtBlack,
        paddingHorizontal: 24,
    },
    wrapUploadImage: {
        marginTop: 10,
        borderWidth: 1,
        borderStyle: 'dashed',
        backgroundColor: Colors.pinkLight,
        borderColor: Colors.pink,
        paddingVertical: 10,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: "center",
    },
    imageUpload: {
        width: 30,
        height: 26
    },
    imgPhoto: {
        width: SCREEN_WIDTH - 76,
        height: SCREEN_WIDTH / 2,
        borderRadius: 15,
        marginTop: 10
    },
    errText: {
        ...Styles.redError10weight500,
        textAlign: 'right',
        marginTop: 3,
        marginRight: 5
    },
    closeContainer: {
        ...Styles.shadow,
        position: 'absolute',
        backgroundColor: Colors.white,
        top: 0,
        right: -5,
        borderRadius: 50,
        padding: 10,
    },

})
