import React from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { SCREEN_WIDTH, PADDING_BOTTOM } from '../common/Configs';
import { Languages } from '../common/Languages';
import { Styles, ButtonStyles } from '../common/Styles';
import Button from '../components/Button';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import ProgressiveImage from '../components/ProgressiveImage';
import { getProducts } from '../firebase/api';
import MyLoading from '../components/MyLoading';
import SessionInfo from '../models/SessionInfo';

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            productList: [],
            isLoading: false
        }
        this.userInfo = SessionInfo.userInfo
    }

    componentDidMount() {
        this.getProducts()
    }

    getProducts = () => {
        this.setState({
            isLoading: true
        })
        getProducts(this.productsRetrieved)
    }

    productsRetrieved = (products) => {
        let arrProduct = []
        products.forEach((val) => {
            if (val.UserId === this.userInfo.uid) {
                arrProduct.push(val)
            }
        });
        this.setState({
            isLoading: false
        }, () => {
            this.setState({
                productList: arrProduct.reverse()
            })
        })

    }



    useCoupon = (item) => {
        Navigator.pushScreen(this, ScreenName.productDetails, { item, requestList: this.getProducts })
    }

    renderCouponItem = ({ item }) => {
        return (
            <View style={styles.wrapItem}>
                <ProgressiveImage
                    source={{ uri: item.UrlImage }}
                    style={styles.wrapImage}
                    styleImage={styles.image}
                    resizeMode="cover"
                />
                <View style={styles.wrapContent}>
                    <Text style={Styles.black20Regular}>{item.ProductName}</Text>
                    <View style={styles.wrapPrice}>
                        <Text style={Styles.pink16bold}>{item.NewPrice}{Languages.common.yen}</Text>
                        {item.OldPrice ? <Text style={styles.txtOldPrice}>{item.OldPrice}{Languages.common.yen}</Text> : null}

                    </View>
                    <Button
                        title={Languages.productList.useCoupon}
                        disabled
                        containerStyle={styles.btn}
                        style={ButtonStyles.pinkBorder}
                        onPress={() => this.useCoupon(item)}
                    />
                </View>

            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <HeaderBar
                    navigation={this.props.navigation}
                    hasBack
                    title={Languages.productList.title}
                />
                <View style={{ flex: 1 }}>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: PADDING_BOTTOM }}
                        data={this.state.productList}
                        renderItem={this.renderCouponItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    {this.state.isLoading ? <MyLoading /> : null}

                </View>

            </View>
        )
    }
}



export default ProductList;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    wrapItem: {
        marginHorizontal: 15,
        marginTop: 15,
        borderRadius: 15
    },
    wrapImage: {
        height: SCREEN_WIDTH / 2,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    image: {
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    wrapContent: {
        paddingVertical: 15,
        paddingHorizontal: 16,
        backgroundColor: Colors.white,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15
    },
    wrapPrice: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5
    },
    txtOldPrice: {
        ...Styles.gray14weight300,
        marginLeft: 30,
        textDecorationLine: 'line-through'
    },
    btn: {
        marginTop: 20,
    }
})
