import React from 'react';
import { StyleSheet, View, Text, TextInput, ScrollView } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { Configs, SCREEN_WIDTH, PADDING_BOTTOM } from '../common/Configs';
import FastImage from 'react-native-fast-image';
import { Languages } from '../common/Languages';
import { Styles, ButtonStyles } from '../common/Styles';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import Icon from '../components/Icon';
import Touchable from '../components/Touchable';
import ProgressiveImage from '../components/ProgressiveImage';
import Button from '../components/Button';
import SimpleTextInput from '../components/SimpleTextInput';
import ScrollViewWithKeyboard from '../components/ScrollViewWithKeyboard';
import Validate from '../utils/Validate';
import { updateProduct, deleteProduct } from '../firebase/api';
import MyLoading from '../components/MyLoading';
import Popup from '../components/Popup';

class ProductDetails extends React.Component {
    constructor(props) {
        super(props);
        this.dataItem = this.props.route.params.item
        this.state = {
            productName: this.dataItem.ProductName,
            couponContent: this.dataItem.CouponContent,
            couponCode: this.dataItem.CouponCode,
            oldPrice: this.dataItem.OldPrice,
            newPrice: this.dataItem.NewPrice,
            error: {},
            isLoading: false,
        }
        this.requestList = this.props.route.params.requestList

    }
    validateData = () => {
        let error = {}
        const { couponContent, productName, couponCode, newPrice, oldPrice } = this.state
        if (Validate.isStringEmpty(couponContent)) {
            error['couponContent'] = Languages.errorMessage.requireFieldCouponContent
        }

        // if (Validate.isStringEmpty(couponCode)) {
        //     error['couponCode'] = Languages.errorMessage.requireFieldCouponCode
        // }

        if (Validate.isStringEmpty(productName)) {
            error['productName'] = Languages.errorMessage.requireFieldProductName
        }

        if (Validate.isStringEmpty(newPrice)) {
            error['newPrice'] = Languages.errorMessage.requireFieldProductPrice
        }

        if (Validate.isStringEmpty(oldPrice)) {
            error['oldPrice'] = Languages.errorMessage.requireFieldProductPrice
        }

        if (Object.keys(error).length > 0) {
            this.setState({ error })
        } else {
            this.setState({ error: {}, isLoading: true })
            this.onUpdate()
        }
    }

    onUpdate = () => {

        this.setState({
            isLoading: true
        })
        const params = {
            UrlImage: this.dataItem.UrlImage,
            ProductName: this.state.productName,
            CouponContent: this.state.couponContent,
            CouponCode: this.state.couponCode,
            NewPrice: this.state.newPrice,
            OldPrice: this.state.oldPrice,
            CreatedAt: new Date(),
            id: this.dataItem.id,
        }
        updateProduct(params, this.updateComplete)
    }

    updateComplete = (updateComplete) => {
        this.setState({
            isLoading: false
        }, () => this.popupConfirm.openPopup())
    }

    requestListAndBack = () => {
        this.requestList()
        Navigator.goBack(this)
    }

    onDelete = () => {
        this.setState({
            isLoading: true
        })
        const params = {
            UrlImage: this.dataItem.UrlImage,
            ProductName: this.state.productName,
            CouponContent: this.state.couponContent,
            CouponCode: this.state.couponCode,
            NewPrice: this.state.newPrice,
            OldPrice: this.state.oldPrice,
            CreatedAt: new Date(),
            id: this.dataItem.id,
        }
        deleteProduct(params, this.deleteProduct)
    }

    deleteProduct = (deleteProduct) => {
        this.setState({
            isLoading: false
        }, () => this.popupDelete.openPopup())
    }

    clickYes = () => {
        this.requestList()
        Navigator.goBack(this)
    }

    clickNo = () => {
        console.log('no')
    }


    render() {
        const { error } = this.state
        return (
            <View style={styles.container}>
                <HeaderBar
                    navigation={this.props.navigation}
                    hasBack
                />
                <ScrollViewWithKeyboard contentContainerStyle={{ paddingBottom: PADDING_BOTTOM }}>
                    <ProgressiveImage
                        source={{ uri: this.dataItem.UrlImage }}
                        style={styles.wrapImage}
                        styleImage={styles.image}
                        resizeMode="cover"
                    />
                    <View style={styles.wrapInfo}>
                        <SimpleTextInput
                            title={Languages.productDetails.productName}
                            containerStyle={[styles.input, { marginTop: 0 }]}
                            style={styles.textInput}
                            maxLength={100}
                            errorMsg={error['productName']}
                            value={this.state.productName}
                            onChangeText={(productName) => this.setState({ productName })}
                        />

                        <SimpleTextInput
                            title={Languages.productDetails.couponContent}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={100}
                            errorMsg={error['couponContent']}
                            value={this.state.couponContent}
                            onChangeText={(couponContent) => this.setState({ couponContent })}
                        />

                        {/* <SimpleTextInput
                            title={Languages.productDetails.couponCode}
                            containerStyle={styles.input}
                            style={styles.textInput}
                            maxLength={6}
                            errorMsg={error['couponCode']}
                            value={this.state.couponCode.toUpperCase()}
                            onChangeText={(couponCode) => this.setState({ couponCode })}
                        /> */}
                        <View style={{ marginHorizontal: 38, marginTop: 15 }}>
                            <Text style={Styles.gray14weight500}>{Languages.productDetails.changeAmount}</Text>
                            <View style={{ flexDirection: 'row', alignItems:'center' }}>
                                <SimpleTextInput
                                    containerStyle={{ flex: 1, marginRight: 10 }}
                                    style={styles.textInput}
                                    maxLength={100}
                                    errorMsg={error['oldPrice']}
                                    keyboardType="numeric"
                                    value={this.state.oldPrice}
                                    onChangeText={(oldPrice) => this.setState({ oldPrice })}
                                />
                                 <Icon icon={require('../assets/images/ic_arrow_right.png')}
                                    color={Colors.txtBlack}
                                    style = {{marginTop: 10}}
                                    size={{ width: Configs.IconSize.size18, height: Configs.IconSize.size20 }} />
                                <SimpleTextInput
                                    containerStyle={{ flex: 1, marginLeft: 10 }}
                                    style={styles.textInput}
                                    maxLength={100}
                                    errorMsg={error['newPrice']}
                                    keyboardType="numeric"
                                    value={this.state.newPrice}
                                    onChangeText={(newPrice) => this.setState({ newPrice })}
                                />
                                
                            </View>
                        </View>

                    </View>
                    <View style={styles.wrapBottom}>
                        <Button
                            title={Languages.productDetails.change}
                            containerStyle={[styles.btn, { marginTop: 30 }]}
                            style={ButtonStyles.pink}
                            onPress={this.validateData}
                        />
                        <Button
                            title={Languages.productDetails.cancel}
                            containerStyle={[styles.btn, { marginBottom: 30 }]}
                            style={ButtonStyles.grayBorder}
                            onPress={this.onDelete}
                        />
                    </View>
                    <Popup
                        ref={ref => this.popupConfirm = ref}
                        mainBtnText={Languages.common.ok}
                        content={Languages.productDetails.success}
                        onMainBtnPress={this.requestListAndBack}
                    />
                    <Popup
                        ref={ref => this.popupDelete = ref}
                        leftBtnText={Languages.common.yes}
                        rightBtnText={Languages.common.no}
                        content={Languages.productDetails.sure}
                        onLeftBtnPress={this.clickYes}
                        onRightBtnPress={this.clickNo}
                    />

                    {this.state.isLoading ? <MyLoading /> : null}
                </ScrollViewWithKeyboard>
            </View>
        )
    }
}



export default ProductDetails;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    wrapImage: {
        height: SCREEN_WIDTH / 2,
        borderRadius: 15,
        marginHorizontal: 15
    },
    image: {
        borderRadius: 15
    },
    wrapInfo: {
        backgroundColor: Colors.white,
        borderRadius: 15,
        justifyContent: 'center',
        marginTop: 30,
        paddingVertical: 30
    },
    wrapBottom: {
    },
    btn: {
        marginHorizontal: 38,
        marginTop: 20
    },

    input: {
        marginHorizontal: 38,
        marginTop: 15,
    },
    textInput: {
        ...Styles.grayLight14weight300,
        color: Colors.txtBlack,
        paddingHorizontal: 24,
    },
})
