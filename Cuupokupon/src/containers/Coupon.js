import React from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { Configs, SCREEN_WIDTH } from '../common/Configs';
import FastImage from 'react-native-fast-image';
import { Languages } from '../common/Languages';
import { Styles } from '../common/Styles';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import Icon from '../components/Icon';
import Touchable from '../components/Touchable';

class Coupon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    gotoProductForm = () => {
        Navigator.pushScreen(this, ScreenName.productForm)
    }

    gotoProductList = () => {
        Navigator.pushScreen(this, ScreenName.productList)
    }

    render() {
        return (
            <View style={styles.container}>
                <HeaderBar
                    navigation={this.props.navigation}
                    hasBack
                />
                <View style={{ flex: 1, justifyContent: 'center' }}>

                    <Touchable
                        onPress={this.gotoProductForm}
                        style={styles.wrapButton}
                    >
                        <View style={styles.wrapImage}>
                            <FastImage
                                style={styles.img}
                                source={require('../assets/images/bg_coupon.png')}
                                resizeMode="cover" />
                        </View>
                        <View style={styles.wrapText}>
                            <Text style={styles.txt}>{Languages.coupon.issueCoupon}</Text>
                        </View>
                        <View style={styles.wrapIcon}>
                            <Icon icon={require('../assets/images/ic_arrow_right_black.png')}
                                color={Colors.txtBlack}
                                size={{ width: Configs.IconSize.size18, height: Configs.IconSize.size20 }} />
                        </View>
                    </Touchable>

                    <Touchable
                        onPress={this.gotoProductList}
                        style={[styles.wrapButton, { marginTop: SCREEN_WIDTH / 3 }]}
                    >
                        <View style={styles.wrapImage}>
                            <FastImage
                                style={styles.img}
                                source={require('../assets/images/bg_edit_coupon.png')}
                                resizeMode="cover" />
                        </View>
                        <View style={styles.wrapText}>
                            <Text style={styles.txt}>{Languages.coupon.editIssueCoupon}</Text>
                        </View>
                        <View style={styles.wrapIcon}>
                            <Icon icon={require('../assets/images/ic_arrow_right_black.png')}
                                color={Colors.txtBlack}
                                size={{ width: Configs.IconSize.size18, height: Configs.IconSize.size20 }} />
                        </View>
                    </Touchable>


                </View>
            </View>
        )
    }
}



export default Coupon;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    wrapButton: {
        borderWidth: 1,
        borderColor: Colors.white,
        backgroundColor: Colors.white,
        marginHorizontal: 38,
        height: SCREEN_WIDTH / 3,
        borderRadius: 15,
        justifyContent: 'flex-end'
    },
    wrapText: {
        height: SCREEN_WIDTH / 6,
        width: "100%",
        alignItems: 'center'
    },
    txt: {
        marginTop: 10,
        ...Styles.black20Bold,
    },
    img: {
        height: SCREEN_WIDTH / 3,
        width: SCREEN_WIDTH / 2,
    },
    wrapImage: {
        height: SCREEN_WIDTH / 3,
        width: "100%",
        alignItems: 'center',
        position: 'absolute',
        top: - SCREEN_WIDTH / 5,
    },
    wrapIcon: {
        position: "absolute",
        right: 20,
        height: SCREEN_WIDTH / 3,
        justifyContent: 'center'
    }

})
