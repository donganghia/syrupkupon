import React from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import HeaderBar from '../components/header/HeaderBar';
import { Colors } from '../common/Colors';
import { SCREEN_WIDTH } from '../common/Configs';
import FastImage from 'react-native-fast-image';
import { Languages } from '../common/Languages';
import { Styles, ButtonStyles, InputStyles } from '../common/Styles';
import Button from '../components/Button';
import SimpleTextInput from '../components/SimpleTextInput';
import Navigator from '../routers/Navigator';
import { ScreenName } from '../common/ScreenName';
import ScrollViewWithKeyboard from '../components/ScrollViewWithKeyboard';
import Validate from '../utils/Validate';
import auth from '@react-native-firebase/auth';
import Popup from '../components/Popup';
import MyLoading from '../components/MyLoading';
import SessionInfo from '../models/SessionInfo';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            error: {},
            isLoading: false
        }
        this.dataLogin = this.props.route.params && this.props.route.params.dataLogin
    }

    componentDidMount() {
        this.initData()
    }

    initData = () => {
        this.setState({
            email: this.dataLogin && this.dataLogin.UserName,
            password: this.dataLogin && this.dataLogin.Password,
        })
    }

    validateData = () => {
        let error = {}
        const { email, password } = this.state
        if (Validate.isStringEmpty(email)) {
            error['email'] = Languages.errorMessage.requireFieldEmail
        } else if (!Validate.isEmail(email)) {
            error['email'] = Languages.errorMessage.invalidEmail
        }

        if (Validate.isStringEmpty(password)) {
            error['password'] = Languages.errorMessage.requireFieldPass
        } else if (!Validate.validatePassWord(password)) {
            error['password'] = Languages.errorMessage.invalidPassword
        }

        if (Object.keys(error).length > 0) {
            this.setState({ error })
        } else {
            this.setState({ error: {}, isLoading: true })
            this.onLogin(email, password)
        }
    }


    onLogin = (email, password) => {
        auth()
            .signInWithEmailAndPassword(email, password)
            .then((data) => {
                SessionInfo.setUserInfo(data.user._user);
                this.setState({ isLoading: false })
                Navigator.pushScreen(this, ScreenName.restaurant)
            })
            .catch(error => {
                this.setState({ isLoading: false })
                this.popupConfirm.openPopup()
            });
    }

    onConfirm = () => {
        console.log('ok')
    }

    gotoSignUp = () => {
        Navigator.pushScreen(this, ScreenName.signup)
    }


    render() {
        const { error } = this.state
        return (
            <View style={styles.container}>
                <HeaderBar
                    noHeader
                />
                <ScrollViewWithKeyboard contentContainerStyle={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={styles.wrapImg}>
                            <FastImage
                                style={styles.img}
                                source={require('../assets/images/bg_login.png')}
                                resizeMode="cover" />
                        </View>
                        <View style={{ flex: 3 }}>
                            <Text style={styles.txtDes}>{Languages.login.desStore}</Text>
                            <SimpleTextInput
                                containerStyle={styles.input}
                                styleType={InputStyles.white}
                                style={styles.textInput}
                                placeholder={Languages.login.email}
                                placeholderTextColor={Colors.txtGrayLight}
                                maxLength={100}
                                value={this.state.email}
                                errorMsg={error['email']}
                                onChangeText={(email) => this.setState({ email })}
                            />

                            <SimpleTextInput
                                containerStyle={styles.input}
                                styleType={InputStyles.white}
                                style={styles.textInput}
                                placeholder={Languages.login.pass}
                                placeholderTextColor={Colors.txtGrayLight}
                                maxLength={20}
                                value={this.state.password}
                                errorMsg={error['password']}
                                isPassword={true}
                                onChangeText={(password) => this.setState({ password })}
                            />
                            <Button
                                title={Languages.login.login}
                                containerStyle={styles.btnLogin}
                                style={ButtonStyles.pinkIcon}
                                onPress={this.validateData}
                            />
                            <Touchable onPress={this.gotoSignUp}>
                                <Text style={styles.signup}>{Languages.login.signup}</Text>
                            </Touchable>
                        </View>
                        <Popup
                            ref={ref => this.popupConfirm = ref}
                            mainBtnText={Languages.common.ok}
                            content={Languages.errorMessage.wrongPassOrEmail}
                            onMainBtnPress={this.onConfirm}
                        />
                        {this.state.isLoading ? <MyLoading /> : null}
                    </View>
                </ScrollViewWithKeyboard>
            </View>
        )
    }
}



export default Login;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary
    },
    wrapImg: {
        alignItems: 'center', justifyContent: 'center'
    },
    img: {
        height: SCREEN_WIDTH / 1.7,
        width: SCREEN_WIDTH / 1.7,
    },
    txtDes: {
        marginTop: 20,
        ...Styles.black18Bold,
        textAlign: 'center'
    },
    btnLogin: {
        marginTop: 20,
        marginHorizontal: 38
    },
    signup: {
        marginTop: 25,
        ...Styles.grayDark16weight300,
        textAlign: "center",
        textDecorationLine: 'underline'
    },

    input: {
        marginHorizontal: 38,
        marginTop: 10,
    },
    textInput: {
        ...Styles.grayLight14weight300,
        color: Colors.txtBlack,
        paddingHorizontal: 24,
    },
})
