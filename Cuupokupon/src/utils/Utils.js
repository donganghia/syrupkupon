import { Platform } from 'react-native';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../common/Configs';


// check if devices is ipX, ipXS, ipXSMAx
getPaddingTopByDevice = () => {
    let padTop = 0;
    if (Platform.OS === 'ios') {
        // check ipX, XS, XS max
        if (SCREEN_WIDTH === 812 || SCREEN_HEIGHT === 812 || SCREEN_WIDTH === 896 || SCREEN_HEIGHT === 896) {
            padTop = 24;
        }
    }
    return padTop;
};

// check if devices is ipX, ipXS, ipXSMAx
getPaddingBottomByDevice = () => {
    let padBottom = 0;
    if (Platform.OS === 'ios') {
        // check ipX, XS, XS max
        if (SCREEN_WIDTH === 812 || SCREEN_HEIGHT === 812 || SCREEN_WIDTH === 896 || SCREEN_HEIGHT === 896) {
            padBottom = 30;
        }
    }
    return padBottom;
};

getDisplayableImagePath = (img) => {
    if (!img) {
        return ''
    } if (img.path) {
        return Platform.OS === 'android' ? 'file://' + img.path : '' + img.path
    } else {
        if (img.indexOf('http') >= 0 && img.indexOf(IMAGES_HOST) < 0) { // social avatar, or other path with http(s)://domain
            return img
        } else {
            if (img.indexOf(IMAGES_HOST) < 0) {
                return IMAGES_HOST + img
            } else {
                return img
            }
        }
    }
}

makeCouponCode = (length) => {
    var result = '';
    var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


export default {
    getPaddingTopByDevice,
    getPaddingBottomByDevice,
    getDisplayableImagePath,
    makeCouponCode
}