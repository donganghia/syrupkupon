import { Alert, Platform, Linking } from 'react-native';

import CheckPermission from '../utils/CheckPermission';
import { Languages } from '../common/Languages';
import ImagePicker from "react-native-image-crop-picker";
import { openSettings } from 'react-native-permissions';

returnSuccess = (response) => {
    return response
}

openCamera = (onSelected) => {
    hasCameraCaptureAndSave(async () => {
        await ImagePicker.openCamera({
            cropping: false,
        }).then(image => {
            onSelected(this.onImageSelected(image))
        }).catch(error => {
            onSelected(null);
        });
    }, (permission) => {
        if (permission.camera !== 'authorized') {
            this.displayPermissionAlert(1, permission.camera, () => this.openCamera(onSelected));
        }
    })
}

openLibrary = async (callback, max) => {
    CheckPermission.requestPhotoLibraryPermission(async () => {
        if (Platform.OS === 'android') {
            await ImagePicker.openPicker({
                multiple: max > 1,
                maxFiles: max,
                mediaType: 'photo'
            })
                .then(response => {
                    let listSl = response
                    if (response.length > max) {
                        listSl = response.splice(0, max);
                    }
                    callback(this.onImageSelected(listSl))
                })
                .catch(error => {
                    callback(null);
                });
        } else {
            const defaultOptions = {
                cropping: false,
                compressImageQuality: 0.5,
                multiple: max > 1,
                maxFiles: max,
                mediaType: 'photo'
            };
            let wrapOptions = Object.assign({}, defaultOptions, {});

            await ImagePicker.openPicker(wrapOptions)
                .then(response => {
                    callback(this.onImageSelected(response))
                })
                .catch(error => {
                    callback(null);
                });
        }
    }, (permission) => {
        this.displayPermissionAlert(0, permission, () => this.openLibrary(callback, max))
    })
}

onImageSelected = (response) => {
    if (!Array.isArray(response)) {
        return [response]
    } else {
        return response
    }
}

displayPermissionAlert = (type, permission = 'denied', callback) => {
    if (permission === 'unavailable') {
        if (type == 1) {
            CheckPermission.requestCameraAccess(callback);
        } else if (type == 0) {
            CheckPermission.requestPhotoAccess(callback);
        }
        return;
    }
    let alertTitle = Languages.image.PermissionAlert, alertMsg = ""
    let btnDeny = Languages.image.Deny
    let btnOpenSetting = Languages.image.OpenSetting
    switch (type) {
        case 0: // photo access
            alertMsg = Languages.image.AccessPhotoMsg;
            break;
        case 1: // camera access
            alertMsg = Languages.image.AccessCameraMsg;
            break;
        case 2: // save image
            alertMsg = Languages.image.AccessAddPhotoMsg;
            break;
        default:
            break;
    }
    Alert.alert(
        alertTitle,
        alertMsg,
        [
            {
                text: btnDeny,
                onPress: () => { },
                style: 'cancel',
            },
            // btn open setting
            // unavailable: chua request, open setting
            permission !== 'unavailable' ?
                {
                    text: btnOpenSetting,
                    onPress: () => {
                        if (Platform.OS === 'ios') {
                            openSettings()
                            // Permissions.canOpenSettings().then((val) => {
                            //     if (val) {
                            //         Permissions.openSettings()
                            //     }
                            // }, (reason) => { }).catch((err) => { })
                        } else {
                            Linking.openSettings()
                        }
                    }
                } :
                { // btn go to setting
                    text: btnOpenSetting,
                    onPress: () => { CheckPermission.requestCameraAccess() }
                },
        ],
    )
}

export default {
    openCamera,
    openLibrary,
    displayPermissionAlert,
}