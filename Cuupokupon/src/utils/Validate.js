
const EMAIL_REGEX = /^[\w+][\w\.\-]+@[\w\-]+(\.\w{2,4})+$/
const PHONE_REGEX = /^0\d{1,4}\d{4}\d{4}$/


isEmpty = (...data) => {
    for (let i = 0; i < data.length; i++) {
        if (!data[i]) return true;
    }
    return false;
}

isStringEmpty = (string) => {
    return !string || string === 'null' || String.prototype.trim.call(string) === '';
}

isObjectEmpty = (obj) => {
    return typeof obj === 'undefined' || obj === null || Object.keys(obj).length === 0;
}

trim = (str) => {
    return isStringEmpty(str) ? '' : String.prototype.trim.call(str)
}
trimAndLowerCase = (str) => {
    return trim(str).toLowerCase()
}
validatePassWord = (value) => {
    return /^\w{6,20}$/.test(value)
}
isEmail = (email) => {
    return EMAIL_REGEX.test(email);
}
validatePassWord = (value) => {
    return /^\w{6,20}$/.test(value)
}

export default {
    isEmpty,
    isStringEmpty,
    isObjectEmpty,
    trim,
    validatePassWord,
    trimAndLowerCase,
    isEmail,
    validatePassWord
}
