import { Platform, Alert, Linking } from 'react-native'
import {
    PERMISSIONS,
    request,
    openSettings
} from 'react-native-permissions';

// request photo access permission
async function requestAll() {
    const writeStorage = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
    const readStorage = await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
    return { writeStorage, readStorage };
}
requestPhotoLibraryPermission = async (onSuccessCallback, onDenyCallback) => {
    if (Platform.OS.toLocaleLowerCase() === 'ios') {
        request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(permission => {
            if (permission === 'granted') {
                onSuccessCallback();
            } else {
                onDenyCallback(permission);
            }
        })
    } else {
        requestAll().then((permission) => {
            if (permission.readStorage === 'granted' && permission.writeStorage === "granted") {
                onSuccessCallback();
            } else {
                onDenyCallback(permission);
            }
        });
    }
}

// check camera access and save
hasCameraCaptureAndSave = async (onSuccessCallback, onDenyCallback) => {
    if (Platform.OS.toLocaleLowerCase() === 'ios') {
        request(PERMISSIONS.IOS.CAMERA).then(permission => {
            if (permission === 'granted') {
                onSuccessCallback();
            } else {
                onDenyCallback(permission);
            }
        })
    } else {
        request(PERMISSIONS.ANDROID.CAMERA).then((permission) => {
            if (permission === 'granted') {
                onSuccessCallback();
            } else {
                onDenyCallback(permission);
            }
        });
    }
}

// request photo access on
requestPhotoAccess = async (callback) => {
    if (Platform.OS.toLocaleLowerCase() === 'ios') {
        request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(response => {
            callback(response);
        })
    } else {
        request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then((response) => {
            callback(response);
        });
    }
}

// request camera access
requestCameraAccess = async (callback) => {
    if (Platform.OS.toLocaleLowerCase() === 'ios') {
        request(PERMISSIONS.IOS.CAMERA).then(response => {
            callback(response);
        })
    } else {
        request(PERMISSIONS.ANDROID.CAMERA).then((response) => {
            callback(response);
        });
    }
}




export default {
    requestCameraAccess,
    requestPhotoAccess,
    requestPhotoLibraryPermission,
    hasCameraCaptureAndSave,
}