
import { debounce } from 'lodash';

const DELAY_TIMER = 200

goBack = debounce((screen) => {
    screen.props.navigation.goBack()
}, DELAY_TIMER, { leading: true, trailing: false })

pushScreen = debounce((screen, destination, data) => {
    screen.props.navigation.push(destination, data)
}, DELAY_TIMER, { leading: true, trailing: false })


replaceScreen = debounce((screen, destination, data) => {
    screen.props.navigation.replace(destination, data)
}, DELAY_TIMER, { leading: true, trailing: false })

navigateScreen = debounce((screen, destination, data) => {
    screen.props.navigation.navigate(destination, data)
}, DELAY_TIMER, { leading: true, trailing: false })


export default {
    goBack,
    pushScreen,
    navigateScreen,
    replaceScreen
};
