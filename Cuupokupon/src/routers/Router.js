import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ScreenName } from '../common/ScreenName';
import Login from '../containers/Login';
import Restaurant from '../containers/Restaurant';
import Coupon from '../containers/Coupon';
import Signup from '../containers/Signup';
import ProductForm from '../containers/ProductForm';
import ProductList from '../containers/ProductList';
import ProductDetails from '../containers/ProductDetails';
import Splash from '../containers/Splash';

const Stack = createStackNavigator();

function MyStack() {
    return (
        <Stack.Navigator headerMode="none">
            <Stack.Screen name={ScreenName.splash} component={Splash} />
            <Stack.Screen name={ScreenName.login} component={Login} />
            <Stack.Screen name={ScreenName.signup} component={Signup} />
            <Stack.Screen name={ScreenName.restaurant} component={Restaurant} />
            <Stack.Screen name={ScreenName.coupon} component={Coupon} />
            <Stack.Screen name={ScreenName.productForm} component={ProductForm} />
            <Stack.Screen name={ScreenName.productList} component={ProductList} />
            <Stack.Screen name={ScreenName.productDetails} component={ProductDetails} />
        </Stack.Navigator>
    );
}


class Router extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <MyStack />
            </NavigationContainer>
        )
    }
}

export default Router;