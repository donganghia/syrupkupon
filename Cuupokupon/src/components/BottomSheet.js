import React, { Component } from 'react'
import MyActionSheet from './actionsheet/MyActionSheet';
import { Languages } from '../common/Languages';
import Validate from '../utils/Validate';
import { Colors } from '../common/Colors';

export default class BottomSheet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listOptions: [],
            selectedIndex: -1,
            isDisplay: false,
            title: ''
        }
        this.listLabel = [];
    }

    show() {
        this.setState({ isDisplay: true }, () => {
            this.asOption.show()
        })
    }

    //extraData: extraData of bottom sheet
    //list: raw data
    //_key, _value are attributes of item (id,name)
    //selected: selected object
    //title: title of bottom sheet
    setData = async (list, selected, title, extraData, _key = 'id', _value = 'value') => {
        let idx = -1
        list.map((val, _) => {
            this.listLabel.push(Validate.trim(val[_value]));
            if (selected && selected[_key] == val[_key]) {
                idx = _
            }
        })
        this.extraData = extraData
        this.title = title
        this._key = _key
        this._value = _value
        this.setState({ 
            listOptions: list, 
            selectedIndex: idx, 
            title: title 
        }, () => this.show())
    }

    onSelectOption = (index) => {
        const data = this.state.listOptions;
        if (index !== data.length) {// select != cancel index
            if (this.props.onSelectOption && data[index]) {
                let selected = {
                    'title': this.title,
                }
                selected[this._key] = data[index][this._key]
                selected[this._value] = Validate.trim(data[index][this._value])
                this.props.onSelectOption(selected, this.extraData, this._key, this._value, data[index]);
            }
        }
        this.listLabel = []
        this.setState({ listOptions: [] })
    }

    getPhotoPickerOptions() {
        return [Languages.image.takeCamera, Languages.image.takeAlbum, Languages.common.cancel]
    }

    render() {
        return this.state.isDisplay ?
            <MyActionSheet
                title={this.state.title}
                ref={b => this.asOption = b}
                bgTitle={Colors.bg_header}
                hideBorderTitle
                selectedIndex={this.state.selectedIndex}
                unSelectedColor={Colors.textBackLight}
                options={this.listLabel}
                listOptions={this.state.listOptions}
                onPress={this.onSelectOption} 
                hasIcon = {this.props.hasIcon}
                /> : null
    }
}
