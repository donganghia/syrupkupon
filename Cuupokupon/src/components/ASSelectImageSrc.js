import React, { Component } from 'react'
import { Languages } from '../common/Languages';
import MyActionSheet from './actionsheet/MyActionSheet';
import ImageUtils from '../utils/ImageUtils';

export default class ASSelectImageSrc extends Component {

    show(maxSelect, key) {
        this.maxSelect = maxSelect ? maxSelect : this.props.maxSelect
        this.key = key
        this.asPhotoPickerOption.show()
    }

    onSelected = (listImg) => {
        this.props.onImageSelected(listImg, this.key)
    }

    getPhotoPickerOptions() {
        return [Languages.image.takeCamera, Languages.image.takeAlbum, Languages.common.cancel]
    }

    selectImageSource = (index) => {
        setTimeout(() => {
            if (index === 0) { // camera
                ImageUtils.openCamera(this.onSelected)
            } else if (index === 1) { // library
                ImageUtils.openLibrary(this.onSelected, this.maxSelect)
            }
        }, 300);
    }

    render() {
        return (
            <MyActionSheet
                ref={b => this.asPhotoPickerOption = b}
                cancelButtonIndex={2}
                options={this.getPhotoPickerOptions()}
                onPress={this.selectImageSource} />
        )
    }
}
