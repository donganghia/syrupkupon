import React from 'react'
import { Platform } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class ScrollViewWithKeyboard extends React.Component {

    scrollToPos = (x, y) => {
        this.scroll.props.scrollToPosition(x, y)
    }

    render() {
        if (Platform.OS === 'ios') {
            return (
                <KeyboardAwareScrollView
                    innerRef={ref => {
                        this.scroll = ref
                    }}
                    refreshControl={this.props.refreshControl}
                    style={[{ width: "100%" }, this.props.style]}
                    contentContainerStyle={this.props.contentContainerStyle}
                    enableOnAndroid={true}
                    behavior={'height'}
                    extraHeight={200}
                    keyboardShouldPersistTaps="handled"
                    automaticallyAdjustContentInsets={false}
                    onKeyboardWillHide={(frames) => {
                    }}
                    scrollEnabled={this.props.scrollEnabled}
                    onScroll={this.props.onScroll}
                    showsVerticalScrollIndicator={!this.props.hidesVerticalScrollIndicator}
                    extraScrollHeight={10}>
                    {this.props.children}
                </KeyboardAwareScrollView>
            )
        }

        return (
            <KeyboardAwareScrollView extraScrollHeight={10}
                innerRef={ref => {
                    this.scroll = ref
                }}
                style={[{ width: "100%" }, this.props.style]}
                behavior={'height'}
                contentContainerStyle={this.props.contentContainerStyle}
                enableOnAndroid={true}
                scrollEnabled={this.props.scrollEnabled}
                refreshControl={this.props.refreshControl}
                showsVerticalScrollIndicator={!this.props.hidesVerticalScrollIndicator}
                keyboardShouldPersistTaps='handled'>
                {this.props.children}
            </KeyboardAwareScrollView>
        )
    }
}
