import { _ } from 'lodash';
import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { Colors } from '../common/Colors';
import Validate from '../utils/Validate';
import Touchable from './Touchable';
import { Styles, InputStyles } from '../common/Styles';

export default class SimpleTextInput extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }


    onPress = () => {
        if (this.inputRef) {
            this.inputRef.focus()
        }
    }



    render() {
        const { title, containerStyle, style, styleType, placeholder, placeholderTextColor, maxLength, keyboardType, onChangeText, value, errorMsg, editable, isPassword } = this.props
        return (
            <View style={containerStyle}>
                {
                    !Validate.isStringEmpty(title) ?
                        <Text style={Styles.gray14weight500}>{title}</Text>
                        : null
                }

                <Touchable
                    style={styleType == InputStyles.white ? styles.borderInputBgWhite : styles.borderInputBgGray}
                    onPress={this.onPress}>
                    <TextInput
                        ref={ref => this.inputRef = ref}
                        placeholder={placeholder}
                        placeholderTextColor={placeholderTextColor}
                        style={style}
                        maxLength={maxLength}
                        keyboardType={keyboardType ? keyboardType : 'default'}
                        returnKeyType='done'
                        value={value}
                        secureTextEntry={isPassword}
                        editable={editable}
                        onChangeText={onChangeText} />
                </Touchable>
                {
                    !Validate.isStringEmpty(errorMsg) ?
                        <Text style={styles.errText}>{errorMsg}</Text>
                        : null
                }

            </View>
        )
    }
}

const styles = StyleSheet.create({
    borderInputBgWhite: {
        marginTop: 10,
        paddingVertical: 10,
        borderWidth: 1,
        borderColor: Colors.white,
        backgroundColor: Colors.white,
        borderRadius: 20
    },
    borderInputBgGray: {
        marginTop: 10,
        paddingVertical: 10,
        borderWidth: 1,
        borderColor: Colors.bgGray,
        backgroundColor: Colors.bgGray,
        borderRadius: 20
    },
    input: {
        marginHorizontal: 38,
        marginTop: 15
    },
    errText: {
        ...Styles.redError10weight500,
        textAlign: 'right',
        marginTop: 3,
        marginRight: 5
    },
})
