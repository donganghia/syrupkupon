import React, { Component } from 'react'
import { Colors } from '../../common/Colors';
import { MARGIN_BOTTOM } from '../../common/Configs';
import ActionSheetCustom from './ActionSheetCustom';
import { Styles } from '../../common/Styles';

export default class MyActionSheet extends Component {

    constructor(props) {
        super(props);
    }

    show = () => {
        this.refs.actionSheet.show()
    }

    render() {
        return (
            <ActionSheetCustom
                ref={'actionSheet'}
                message={this.props.message}
                options={this.props.options}
                listOptions={this.props.listOptions}
                cancelButtonIndex={this.props.cancelButtonIndex}
                destructiveButtonIndex={this.props.cancelButtonIndex}
                bgTitle={this.props.bgTitle}
                hideBorderTitle={this.props.hideBorderTitle}
                tintColor={this.props.unSelectedColor ? this.props.unSelectedColor : Colors.pink}
                title={this.props.title}
                styles={{
                    body: { marginBottom: MARGIN_BOTTOM },
                    messageBox: { height: 50 },
                    buttonText: { ...Styles.blackLight13Regular, paddingHorizontal: 10 }
                }}
                selectedIndex={this.props.selectedIndex}
                onPress={this.props.onPress}
                hasIcon={this.props.hasIcon}
            />
        );
    }
}