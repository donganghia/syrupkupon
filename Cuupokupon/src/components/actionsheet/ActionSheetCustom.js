import React from 'react';
import { Animated, Dimensions, Easing, Modal, ScrollView, Text, TouchableHighlight, View, FlatList } from 'react-native';
import { Colors } from '../../common/Colors';
import { Styles } from '../../common/Styles';
import styles2 from './styles';
import * as utils from './utils';
import Icon from '../Icon';
import { PADDING_BOTTOM } from '../../common/Configs';

const WARN_COLOR = '#FF3B30'
const MAX_HEIGHT = Dimensions.get('window').height * 0.6

class ActionSheetCustom extends React.Component {
    static defaultProps = {
        tintColor: Colors.primary,
        buttonUnderlayColor: '#F4F4F4',
        onPress: () => { },
        styles: {}
    }

    constructor(props) {
        super(props)
        this.scrollEnabled = false
        this.translateY = this._calculateHeight(props)
        this.state = {
            visible: false,
            sheetAnim: new Animated.Value(this.translateY)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        this.translateY = this._calculateHeight(this.props)
    }

    get styles() {
        const { styles } = this.props
        const obj = {}
        Object.keys(styles2).forEach((key) => {
            const arr = [styles2[key]]
            if (styles[key]) {
                arr.push(styles[key])
            }
            obj[key] = arr
        })
        return obj
    }

    show = () => {
        this.setState({ visible: true }, () => {
            this._showSheet()
        })
    }

    hide = (index) => {
        this._hideSheet(() => {
            this.setState({ visible: false }, () => {
                this.props.onPress(index)
            })
        })
    }

    _cancel = () => {
        const { cancelButtonIndex } = this.props
        // 保持和 ActionSheetIOS 一致，
        // 未设置 cancelButtonIndex 时，点击背景不隐藏 ActionSheet
        if (utils.isset(cancelButtonIndex)) {
            this.hide(cancelButtonIndex)
        } else {
            this.hide(1000)
        }
    }

    _showSheet = () => {
        Animated.timing(this.state.sheetAnim, {
            toValue: 0,
            duration: 250,
            useNativeDriver: true,
            easing: Easing.out(Easing.ease)
        }).start()
    }

    _hideSheet(callback) {
        Animated.timing(this.state.sheetAnim, {
            toValue: this.translateY,
            duration: 200,
            useNativeDriver: true
        }).start(callback)
    }

    /**
     * elements: titleBox, messageBox, buttonBox, cancelButtonBox
     * box size: height, marginTop, marginBottom
     */
    _calculateHeight(props) {
        const styles = this.styles

        const getHeight = (name) => {
            const style = styles[name][styles[name].length - 1]
            let h = 0
                ;['height', 'marginTop', 'marginBottom'].forEach((attrName) => {
                    if (typeof style[attrName] !== 'undefined') {
                        h += style[attrName]
                    }
                })
            return h
        }

        let height = 0
        if (props.title) height += getHeight('titleBox')
        if (props.message) height += getHeight('messageBox')
        if (utils.isset(props.cancelButtonIndex)) {
            height += getHeight('cancelButtonBox')
            height += (props.options.length - 1) * getHeight('buttonBox')
        } else {
            height += props.options.length * getHeight('buttonBox')
        }

        if (height > MAX_HEIGHT) {
            this.scrollEnabled = true
            height = MAX_HEIGHT
        } else {
            this.scrollEnabled = false
        }

        return height
    }

    _renderTitle() {
        const { title } = this.props
        const style = {
            justifyContent: 'flex-start',
            paddingVertical: 10,
            paddingHorizontal: 20,
            backgroundColor: this.props.bgTitle ? this.props.bgTitle : Colors.white,
            borderBottomColor: this.props.hideBorderTitle ? Colors.transparent : Colors.grayLight,
            borderBottomWidth: this.props.hideBorderTitle ? 0 : 2
        }
        if (!title) return null
        return (
            <View style={style}>
                {React.isValidElement(title) ? title : (
                    <Text style={Styles.black15Bold}>{title}</Text>
                )}
            </View>
        )
    }

    _renderMessage() {
        const { message } = this.props
        const styles = this.styles
        if (!message) return null
        return (
            <View style={styles.messageBox}>
                {React.isValidElement(message) ? message : (
                    <Text style={styles.messageText}>{message}</Text>
                )}
            </View>
        )
    }

    _renderCancelButton() {
        const { options, cancelButtonIndex } = this.props
        if (!utils.isset(cancelButtonIndex)) return null
        return this._createButton(options[cancelButtonIndex], cancelButtonIndex)
    }

    _createButton(title, index) {
        const styles = this.styles
        const { buttonUnderlayColor, cancelButtonIndex, destructiveButtonIndex, tintColor, selectedIndex } = this.props
        const fontColor = destructiveButtonIndex === index ? WARN_COLOR : (selectedIndex === index ? Colors.primary : tintColor)
        const buttonBoxStyle = cancelButtonIndex === index ? styles.cancelButtonBox : styles.buttonBox
        const leftStyle = cancelButtonIndex === index ? {} : { textAlign: 'left', alignSelf: 'flex-start' };
        const selectedStyle = (index === selectedIndex) ? { justifyContent: 'space-between', alignItems: 'flex-start' } : (index === cancelButtonIndex) ? { alignItems: 'center', justifyContent: 'center' } : {}
        return (
            <TouchableHighlight
                key={index}
                activeOpacity={1}
                underlayColor={buttonUnderlayColor}
                style={[buttonBoxStyle, { justifyContent: 'center', alignItems: 'flex-start' }]}
                onPress={() => this.hide(index)}
            >
                <View style={[selectedStyle, { width: '100%', flexDirection: 'row', paddingHorizontal: 10 }]}>
                    {React.isValidElement(title) ? title : (
                        <Text style={[styles.buttonText, { color: fontColor }, leftStyle]}>{title}</Text>
                    )}
                    {/* {selectedIndex === index ?
                        <Icon icon={require('../../assets/images/ic_tick.png')}
                        /> : null} */}
                </View>
            </TouchableHighlight>
        )
    }

    _renderOptions() {
        const { cancelButtonIndex } = this.props
        return this.props.options.map((title, index) => {
            return cancelButtonIndex === index ? null : this._createButton(title, index)
        })
    }

    _renderOptionsWithIcon() {
        const styles = this.styles
        const { buttonUnderlayColor, cancelButtonIndex, destructiveButtonIndex, tintColor, selectedIndex } = this.props
        return (
            <FlatList
                data={this.props.listOptions}
                renderItem={({ item, index }) => {
                    return (
                        <TouchableHighlight
                            key={index}
                            activeOpacity={1}
                            underlayColor={buttonUnderlayColor}
                            style={[cancelButtonIndex === index ? styles.cancelButtonBox : styles.buttonBox, { justifyContent: 'center', alignItems: 'flex-start' }]}
                            onPress={() => this.hide(index)}
                        >
                            <View style={{ width: '100%', flexDirection: 'row', paddingHorizontal: 10 }}>
                                <Icon icon={item.source}
                                    color={selectedIndex == index ? Colors.blue : Colors.textBackLight}
                                />
                                <Text style={[selectedIndex == index ? Styles.pink14bold : Styles.gray14weight500, { marginLeft: 10 }]}>{item.name}</Text>

                            </View>
                        </TouchableHighlight>
                    )
                }}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    render() {
        const styles = this.styles
        const { visible, sheetAnim } = this.state
        return (
            <Modal visible={visible}
                animationType='none'
                transparent
                onRequestClose={this._cancel}
            >
                <View style={[styles.wrapper, { marginBottom: PADDING_BOTTOM }]}>
                    <Text
                        style={[styles.overlay]}
                        onPress={this._cancel}
                    />
                    <Animated.View
                        style={[
                            styles.body,
                            { height: this.translateY, transform: [{ translateY: sheetAnim }] }
                        ]}
                    >
                        {this._renderTitle()}
                        {this._renderMessage()}
                        <ScrollView scrollEnabled={this.scrollEnabled}>{this.props.hasIcon ? this._renderOptionsWithIcon() : this._renderOptions()}</ScrollView>
                        {this._renderCancelButton()}
                    </Animated.View>
                </View>
            </Modal>
        )
    }
}

export default ActionSheetCustom
