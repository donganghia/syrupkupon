import React from 'react';
import { StatusBar, StyleSheet, Text, View, Platform, Image } from 'react-native';
import { Colors } from '../../common/Colors';
import { Configs } from '../../common/Configs';
import GeneralStatusBarColor from './GeneralStatusBarColor';
import Touchable from '../Touchable';
import { Styles } from '../../common/Styles';

class HeaderBar extends React.Component {

    constructor(props) {
        super(props);

    }

    onBackPressed = () => {
        this.props.onBackPressed ? this.props.onBackPressed() : this.props.navigation.goBack()
    }

    renderTitle = () => {
        return (
            <View style={styles.headerContainer}>
                {
                    this.props.hasBack ?
                        <Touchable onPress={this.onBackPressed}>
                            <Image source={require('../../assets/images/ic_back.png')}
                                style={styles.img}
                                resizeMode="contain" />
                        </Touchable> : null
                }
                <View style={styles.wrapTitle}>
                    <Text style={[styles.title, this.props.hasBack ? { marginLeft: - 40 } : null]}>{this.props.title}</Text>
                </View>
            </View>)
    }

    render() {
        return (
            <View>
                <GeneralStatusBarColor
                    // backgroundColor={Colors.primary}
                    barStyle={"light-content"}
                />
                {!this.props.noHeader ? this.renderTitle() : null}
            </View>


        )
    }
}

export default HeaderBar;

const styles = StyleSheet.create({
    headerContainer: {
        width: '100%',
        height: Configs.heightHeader,
        backgroundColor: Colors.primary,
        flexDirection: 'row',
        alignItems: 'center'
    },
    img: {
        height: 20,
        width: 20,
        tintColor: Colors.txtBlack,
        marginHorizontal: 20
    },
    wrapTitle: {
        flex: 1,
        alignItems: "center",
        justifyContent: 'center',
    },
    title: {
        ...Styles.black20Bold
    }
});
