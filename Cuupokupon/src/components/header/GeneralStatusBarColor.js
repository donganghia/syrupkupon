//custom statusbar

import React from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import { STATUSBAR_HEIGHT, PADDING_TOP } from '../../common/Configs';

const GeneralStatusBarColor = ({ backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar
            translucent
            backgroundColor={backgroundColor}
            {...props} />
    </View>
);
export default GeneralStatusBarColor;

const styles = StyleSheet.create({
    statusBar: {
        paddingTop: STATUSBAR_HEIGHT + PADDING_TOP,
    },
});

