import React from 'react'
import { Image } from 'react-native';
import { Configs } from '../common/Configs';
import Touchable from './Touchable'

class Icon extends React.Component {
    onPress = () => {
        if (this.props.onPress) {
            this.props.onPress()
        }
    }

    render() {
        let defaultSize = Configs.IconSize.size20
        let iconStyle

        if (this.props.size) {
            if (typeof this.props.size === 'object') {
                iconStyle = {
                    width: this.props.size.width ? this.props.size.width : defaultSize,
                    height: this.props.size.height ? this.props.size.height : defaultSize,
                }
            } else {
                iconStyle = {
                    width: this.props.size,
                    height: this.props.size,
                }
            }
        } else {
            iconStyle = {
                width: defaultSize,
                height: defaultSize,
            }
        }
        if (this.props.color) {
            iconStyle.tintColor = this.props.color
        }

        if (this.props.onPress) {
            return <Touchable
                style={this.props.style}
                onPress={this.onPress} >
                <Image
                    style={iconStyle}
                    resizeMode={'contain'}
                    source={this.props.icon}
                />
            </Touchable>
        } else {
            return <Image
                style={[this.props.style, iconStyle]}
                resizeMode={'contain'}
                source={this.props.icon}
            />
        }

    }
}

export default Icon