import React from 'react';
import {
    View,
    ActivityIndicator,
    StyleSheet,
} from 'react-native';
import { Colors } from '../common/Colors';

const MyLoading = (props) => (
    props.isSmall ?
        <View style={styles.loadingWrap}>
            <ActivityIndicator size="small" color={Colors.red_error} />
        </View> :
        <View style={[styles.progressBar]}>
            <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.red_error} />
        </View>
);

const styles = StyleSheet.create({
    progressBar: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 10000,
        opacity: 0.6,
        width: '100%',
        height: '100%'
    },
    activityIndicator: {
        width: 60,
        height: 60,
        backgroundColor: '#e1e4e8',
        borderRadius: 5
    },
    loadingWrap: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    },
});

export default MyLoading;
