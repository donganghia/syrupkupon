import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Modal from 'react-native-modal';
import { Colors } from '../common/Colors';
import { Styles, ButtonStyles } from '../common/Styles';
import Button from './Button';
import { SCREEN_WIDTH } from '../common/Configs';

class Popup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDisplay: false,
        };
    }

    componentDidMount() {
        this._isMounted = true
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    openPopup(data) {
        this.data = data
        this.setState({ isDisplay: true });
    }

    closePopupAuto() {
        this.setState({ isDisplay: false });
    }

    closePopup = async () => {
        this.setState({ isDisplay: false });
        setTimeout(() => {
            if (this.props.onClose) {
                this.props.onClose();
            }
        }, 300);
    };

    onLeftBtnPress = () => {
        this.closePopupAuto()
        if (this.props.onLeftBtnPress) {
            this.props.onLeftBtnPress(this.data)
        }
    }

    onRightBtnPress = () => {
        this.closePopupAuto()
        if (this.props.onRightBtnPress) {
            this.props.onRightBtnPress(this.data)
        }
    }

    onMainBtnPress = () => {
        this.closePopupAuto()
        if (this.props.onMainBtnPress) {
            this.props.onMainBtnPress(this.data)
        }
    }

    onPressOutside = () => {
        if (this.props.canceledOnTouchOutside) {
            this.closePopupAuto()
        }
    }

    renderPopupContent() {
        let content = []
        content.push(this.renderContent())
        content.push(this.renderButtons())
        return content
    }

    renderContent() {
        let content = this.props.content
        if (this.data && this.data.content) {
            content = this.data.content
        }
        return (this.props.contentView ? this.props.contentView :
            <Text style={styles.content} key="popupMsg">{content}</Text>)
    }

    renderButtons() {
        const containerStyle = { flex: 1 }
        let leftBtnText = this.props.leftBtnText
        if (this.data && this.data.leftBtnText) {
            leftBtnText = this.data.leftBtnText
        }

        return (<View style={[styles.btnContainer, { flexDirection: 'row' }]}
            key="popupButtons">
            {leftBtnText ? <Button
                containerStyle={[styles.leftBtn, containerStyle]}
                style={ButtonStyles.pinkBorder}
                title={leftBtnText}
                textAllCaps
                onPress={this.onLeftBtnPress} /> : null}
            {this.props.rightBtnText ? <Button
                containerStyle={[styles.rightBtn, containerStyle]}
                style={ButtonStyles.pink}
                title={this.props.rightBtnText}
                textAllCaps
                onPress={this.onRightBtnPress} /> : null}
            {this.props.mainBtnText ? <Button
                containerStyle={[styles.mainBtn, containerStyle]}
                style={ButtonStyles.pink}
                title={this.props.mainBtnText}
                textAllCaps
                onPress={this.onMainBtnPress} /> : null}
        </View>);
    }

    render() {
        return (
            <Modal
                isVisible={this.state.isDisplay}
                animationIn="slideInUp"
                useNativeDriver={true}
                onRequestClose={this.onPressOutside}
                onBackdropPress={this.onPressOutside}
                avoidKeyboard={true}
            >
                <View style={styles.popup}>
                    <View style={styles.popupContent}>
                        {this.renderPopupContent()}
                    </View>
                </View>
            </Modal>
        );
    }
}

export default Popup;

const styles = StyleSheet.create({
    popup: {
        padding: 15,
        borderRadius: 15
    },
    popupContent: {
        ...Styles.shadow,
        backgroundColor: Colors.white,
        borderRadius: 15
    },
    closeBtnContainer: {
        ...Styles.shadow,
        position: 'absolute',
        backgroundColor: Colors.white,
        top: 0,
        right: 0,
        borderRadius: 50,
        padding: 10,
    },
    content: {
        marginTop: 20,
        ...Styles.black14weight500,
        textAlign: 'center',
        paddingHorizontal: 5,
        paddingVertical: 10,
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        marginBottom: 5,
        padding: 10,
    },
    leftBtn: {
        marginBottom: 10,
        marginHorizontal: 10,
    },
    rightBtn: {
        marginBottom: 10,
        marginHorizontal: 10,
    },
    mainBtn: {
        marginBottom: 10,
        marginHorizontal: SCREEN_WIDTH / 5,
    }
});

