import { _ } from 'lodash';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Colors } from '../common/Colors';
import { Configs } from '../common/Configs';
import { Constants } from '../common/Constants';
import { ButtonStyles, Styles } from '../common/Styles';
import Icon from './Icon';

export default class Button extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    getStyle() {
        switch (this.props.style) {
            case ButtonStyles.pink:
                return [buttonStyle.container, { backgroundColor: Colors.pink }]
            case ButtonStyles.pinkBorder:
                return [buttonStyle.container, { backgroundColor: Colors.white, borderColor: Colors.pink, borderWidth: 1 }]
            case ButtonStyles.grayBorder:
                return [buttonStyle.container, { backgroundColor: Colors.primary, borderColor: Colors.txtGray, borderWidth: 1 }]
            case ButtonStyles.pinkIcon:
                return [buttonStyle.container, { backgroundColor: Colors.pink }]
        }
    }

    getTextStyle() {
        let style = _.cloneDeep(Styles.txtButton)
        switch (this.props.style) {
            case ButtonStyles.pink:
                style.color = Colors.white
                break;
            case ButtonStyles.pinkBorder:
                style.color = Colors.pink
                break;
            case ButtonStyles.pinkIcon:
                style.color = Colors.white
                break;
            case ButtonStyles.grayBorder:
                style.color = Colors.txtBlackLight
                break;
        }
        return style
    }

    onPress = () => {
        if (!this.props.disabled && this.props.onPress) { this.props.onPress() }
    }

    renderContent() {
        let textStyle = this.getTextStyle()
        switch (this.props.style) {
            case ButtonStyles.pink:
                return (<Text style={[textStyle, this.props.titleStyle]}>
                    {this.props.title}
                </Text>)
            case ButtonStyles.pinkBorder:
                return (<Text style={[textStyle, this.props.titleStyle]}>
                    {this.props.title}
                </Text>)
            case ButtonStyles.grayBorder:
                return (<Text style={[textStyle, this.props.titleStyle]}>
                    {this.props.title}
                </Text>)
            case ButtonStyles.pinkIcon:
                return (<View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 25, flex: 1 }}>
                    <Text style={[textStyle, this.props.titleStyle]}>
                        {this.props.title}
                    </Text>
                    <Icon icon={require('../assets/images/ic_arrow_right.png')}
                        color={Colors.white}
                        size={{ width: Configs.IconSize.size18, height: Configs.IconSize.size20 }} />
                </View>)




        }
    }

    render() {
        let container = this.getStyle()

        return (<TouchableOpacity style={[container, this.props.containerStyle]}
            activeOpacity={this.props.disabled ? 1 : 0.8}
            onPress={this.onPress}>
            {this.renderContent()}
        </TouchableOpacity>)
    }
}

const buttonStyle = StyleSheet.create({
    container: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
    }
})
