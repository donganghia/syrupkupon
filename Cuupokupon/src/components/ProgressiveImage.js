import React from 'react';
import { View, StyleSheet, Animated } from 'react-native';

class ProgressiveImage extends React.PureComponent {
    thumbnailAnimated = new Animated.Value(0);

    imageAnimated = new Animated.Value(0);

    handleThumbnailLoad = () => {
        Animated.timing(this.thumbnailAnimated, {
            toValue: 1,
            useNativeDriver: true
        }).start();
    }

    onImageLoad = () => {
        Animated.timing(this.imageAnimated, {
            toValue: 1,
            useNativeDriver: true
        }).start();
    }

    render() {
        const {
            thumbnailSource,
            source,
            style,
            styleImage,
            ...props
        } = this.props;

        return (
            <View style={[styles.container, style]}>
                <Animated.Image
                    {...props}
                    source={thumbnailSource}
                    style={[style, { opacity: this.thumbnailAnimated }]}
                    onLoad={this.handleThumbnailLoad}
                    blurRadius={1}
                />
                <Animated.Image
                    {...props}
                    source={source}
                    style={[styles.imageOverlay, { opacity: this.imageAnimated }, styleImage]}
                    onLoad={this.onImageLoad}
                />
            </View>
        );
    }
}

export default ProgressiveImage;

const styles = StyleSheet.create({
    imageOverlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
    },
    container: {
        backgroundColor: '#e1e4e8',
    },
});
