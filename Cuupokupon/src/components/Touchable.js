import React from 'react';
import { TouchableOpacity } from 'react-native';

export default Touchable = (props) => {
    return (
        
        <TouchableOpacity style={props.style}
            activeOpacity={props.onPress ? 0.8 : 1}
            onPress={props.onPress}>
            {props.children}
        </TouchableOpacity>
    )
}