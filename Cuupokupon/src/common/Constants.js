export const Constants = {
    
    EVENT: {   
        UPDATE_LIST: "UPDATE_LIST"
    },
    COLLECTION: {
        RESTAURANT: "restaurant",
        PRODUCT: "product"
    },
    StorageKey: {
        KEY_USER_INFO: 'KEY_USER_INFO',
    }
}
