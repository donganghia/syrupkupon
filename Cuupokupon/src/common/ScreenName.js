export const ScreenName = {
    splash: "Splash",
    coupon: "Coupon",
    productForm: "ProductForm",
    productList: "ProductList",
    login: "Login",
    signup: "Signup",
    restaurant: "Restaurant",
    productDetails: "ProductDetails",
}