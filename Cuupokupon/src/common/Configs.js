import { Dimensions, StatusBar } from "react-native";
import Utils from "../utils/Utils";

export const SCREEN_HEIGHT = Dimensions.get('window').height;
export const SCREEN_WIDTH = Dimensions.get('window').width;
export const PADDING_BOTTOM = Utils.getPaddingBottomByDevice()
export const PADDING_TOP = Utils.getPaddingTopByDevice()
export const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export const Configs = {
    heightHeader: 50,
    FontFamily: {
        regular: 'NotoSans-Regular',
        bold: 'NotoSans-Bold',
        semiBold: 'NotoSans-SemiBold',
        medium: 'NotoSans-Medium',
    },
    IconSize: {
        size8: 8,
        size10: 10,
        size12: 12,
        size13: 13,
        size14: 14,
        size15: 15,
        size16: 16,
        size17: 17,
        size18: 18,
        size20: 20,
        size22: 22,
        size25: 25,
        size28: 28,
        size30: 30,
        size32: 32,
        size35: 35,
        size44: 44,
        size80: 80,
        size120: 120,
    },
    FontSize: {
        size7: 7,
        size8: 8,
        size9: 9,
        size10: 10,
        size11: 11,
        size12: 12,
        size13: 13,
        size14: 14,
        size15: 15,
        size16: 16,
        size17: 17,
        size18: 18,
        size19: 19,
        size20: 20,
        size22: 22,
        size24: 24,
        size30: 30,
        size32: 32,
    },
}




