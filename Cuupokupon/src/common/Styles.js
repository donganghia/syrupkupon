import { Colors } from "./Colors";
import { Configs } from "./Configs";


export const ButtonStyles = {
    pink: 0,
    pinkIcon: 1,
    pinkBorder: 2,
    grayBorder: 3,
}
export const InputStyles = {
    gray: 0,
    white: 1,
}

export const Styles = {
    shadow: {
        borderRadius: 8,
        backgroundColor: Colors.white,
        shadowColor: Colors.txtBlack,
        shadowOffset: { width: 0, height: 1, },
        shadowOpacity: 0.25,
        shadowRadius: 2,
        elevation: 2,
    },
    txtButton: {
        color: Colors.white,
        fontSize: Configs.FontSize.size16,
        fontFamily: Configs.FontFamily.bold,
    },

    redError10weight500: {
        fontFamily: Configs.FontFamily.semiBold,
        fontSize: Configs.FontSize.size10,
        color: Colors.red_error,
    },

    //back
    black20Bold: {
        color: Colors.txtBlack,
        fontSize: Configs.FontSize.size20,
        fontFamily: Configs.FontFamily.bold,
    },
    black18Bold: {
        color: Colors.txtBlack,
        fontSize: Configs.FontSize.size18,
        fontFamily: Configs.FontFamily.bold,
    },
    black15Bold: {
        color: Colors.txtBlack,
        fontSize: Configs.FontSize.size15,
        fontFamily: Configs.FontFamily.bold,
    },
    black14weight500: {
        color: Colors.txtBlack,
        fontSize: Configs.FontSize.size14,
        fontFamily: Configs.FontFamily.medium,
    },
    black20Regular: {
        color: Colors.txtBlack,
        fontSize: Configs.FontSize.size20,
        fontFamily: Configs.FontFamily.regular,
    },
    //gray
    gray14weight500: {
        color: Colors.txtGray,
        fontSize: Configs.FontSize.size14,
        fontFamily: Configs.FontFamily.semiBold,
    },
    grayDark16weight300: {
        color: Colors.txtGrayDark,
        fontSize: Configs.FontSize.size16,
        fontFamily: Configs.FontFamily.medium,
    },
    grayLight14weight300: {
        color: Colors.txtGrayLight,
        fontSize: Configs.FontSize.size14,
        fontFamily: Configs.FontFamily.medium,
    },
    gray14weight300: {
        color: Colors.txtGray,
        fontSize: Configs.FontSize.size14,
        fontFamily: Configs.FontFamily.medium,
    },

    //white
    white16bold: {
        color: Colors.white,
        fontSize: Configs.FontSize.size16,
        fontFamily: Configs.FontFamily.bold,
    },

    //pink
    pink16bold: {
        color: Colors.pink,
        fontSize: Configs.FontSize.size16,
        fontFamily: Configs.FontFamily.bold,
    },
    pink14bold: {
        color: Colors.pink,
        fontSize: Configs.FontSize.size14,
        fontFamily: Configs.FontFamily.bold,
    },
}