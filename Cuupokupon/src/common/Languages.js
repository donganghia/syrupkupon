export const Languages = {
    common: {
        yen: "円",
        cancel: "キャンセル",
        ok: "OK",
        yes: "はい",
        no: "いいえ"
    },
    login: {
        desStore: "こちらはお店用のアカウントです",
        login: "ログイン",
        register: "登録",
        email: "メールアドレス",
        pass: "パスワード",
        rePass: "パスワード確認",
        signup: "新規登録"
    },
    // signup: {
    //     emailAlreadyInUse: "メールアドレスが登録されました。"
    // },
    restaurant: {
        restaurantInfo: "お店情報の入力",
        complete: "完了",
        cancel: "キャンセル",
        shopPhoto: "お店の写真",
        shopName: "お店の名前",
        shopIcon: "お店のアイコン",
        address: "住所",
        urlMenu: "メニューURL"
    },
    coupon: {
        issueCoupon: "商品を投稿する",
        editIssueCoupon: "投稿した商品を編集する"
    },
    productList: {
        title: "商品を編集する",
        useCoupon: "商品を編集する",
    },
    productForm: {
        title: "商品情報の入力",
        productPhotos: "商品の写真",
        productName: "商品名",
        couponContent: "商品内容",
        couponCode: "商品番号",
        changeAmount: "金額（円）",
        post: "投稿する",
        cancel: "キャンセル",
        success: "投稿が完了しました！"
    },
    productDetails: {
        change: "変更する",
        cancel: "削除する",
        productName: "商品名",
        couponContent: "商品内容",
        couponCode: "商品番号",
        changeAmount: "金額の変化（円）",
        sure: "本当に削除しますか？",
        success: "変更が完了しました！"
    },
    image: {
        takeAlbum: 'ライブラリから選択',
        takeCamera: '写真を撮る',
        PermissionAlert: "パーミッション",
        AccessPhotoMsg: "Cuupoが写真へのアクセスを求めています",
        AccessCameraMsg: "Cuupoがカメラへのアクセスを求めています。",
        AccessAddPhotoMsg: "Cuupoがライブラリへの写真保存を求めています",
        OpenSetting: "セッティングを開きますか",
        Deny: "許可しない",
        uploading: ['Uploading %ith image...', 'Uploading %ist image...', 'Uploading %ind image...',
            'Uploading %ird image...'],
        cover: 'カバー'
    },
    errorMessage: {
        // login
        requireFieldEmail: "メールアドレスを入力されていません。",
        requireFieldPass: "パスワードを入力されていません。",
        invalidEmail: "メールアドレスが間違っています。",
        invalidPassword: "パスワードは6文字以上20文字以下で入力してください。",
        notMatchPass: "確認用パスワードと一致していません。",
        existEmail: "メールアドレスが登録されました。",
        wrongPassOrEmail: "メールアドレスまたはパスワードが間違っています。",

        requireFieldShopName: "お店の名前を入力されていません。",
        requireFieldAddress: "住所を入力されていません。",
        requireFieldShopPhoto: "お店の写真がありません。",
        requireFieldShopIcon: "お店のアイコンがありません。",

        requireFieldProductImage: "商品の写真の写真がありません。",
        requireFieldCouponCode: " 商品番号を入力されていません。",
        requireFieldCouponContent: " 商品内容を入力されていません。",
        requireFieldProductName: " 商品名を入力されていません。",
        requireFieldProductPrice: "金額の変化を入力されていません。",
        uploading: "写真のアップロード中"


    }
}