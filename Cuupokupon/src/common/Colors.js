export const Colors = {
    primary: '#F0DED2',
    white: "#FFFFFF",
    txtBlack: "#333333",
    txtBlackLight: "#4F4F4F",
    txtGray: "#828282",
    txtGrayDark: "#737373",
    txtGrayLight: "#A7A7A7",
    pink: "#F36B7F",
    pinkLight: "#FFF1F3",
    red_error: "#FF5555",
    bgGray: "#F2F2F2"
};
