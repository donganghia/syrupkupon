import AsyncStorage from "@react-native-community/async-storage";
import { Constants } from "../common/Constants";
import StorageManager from "./StorageManager";


class SessionInfo {

    static userInfo = {}

    initData(callback) {
        let keys = [
            Constants.StorageKey.KEY_USER_INFO,
        ]
        AsyncStorage.multiGet(keys, (err, stores) => {
            stores.map((result, i, store) => {
                if (store[i][0] == Constants.StorageKey.KEY_USER_INFO) {
                    try {
                        this.userInfo = JSON.parse(store[i][1])
                    } catch (e) {
                    }
                }
            })
            callback()
        })
    }


    setUserInfo(userInfo) {
        this.userInfo = userInfo
        StorageManager.saveDataToKey(Constants.StorageKey.KEY_USER_INFO, JSON.stringify(this.userInfo))
    }


    getUserInfo() {
        return this.userInfo
    }



    logout() {
        this.setUserInfo(null)
    }
}

export default new SessionInfo();
