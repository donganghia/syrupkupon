
import AsyncStorage from '@react-native-community/async-storage';

// get saved data by key
getDataByKeyWithCallback = (key, callback) => {
    AsyncStorage.getItem(key).then((value) => {
        // console.log("get key " + key + ' with data ' + value);
        if (value != null) {
            callback(JSON.parse(value))
        } else {
            callback(null)
        }
    });
}

// save data
saveDataToKey = (key, data) => {
    try {
        AsyncStorage.setItem(key, data);
    } catch (error) {
        console.log(error);
    }
}

// get data from key
getDataByKey = async (key) => {
    return await AsyncStorage.getItem(key);
}

clearDataOfKey = (key) => {
    try {
        AsyncStorage.removeItem(key);
    } catch (error) {
        console.log(error);
    }
}

export default {
    getDataByKeyWithCallback,
    saveDataToKey,
    getDataByKey,
    clearDataOfKey,
}
