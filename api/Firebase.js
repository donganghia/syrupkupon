import firebase from 'firebase';

const config = {
    apiKey: 'AIzaSyCHIqmJq7UnK3zOnNgg2FKUi3FRNcJMxcw',
	authDomain: 'syrupkupon.firebaseapp.com',
	databaseURL: 'https://syrupkupon.firebaseio.com',
	projectId: 'syrupkupon',
	storageBucket: 'syrupkupon.appspot.com',
	messagingSenderId: '685659107293'
};

firebase.initializeApp(config);

export default firebase;

export const database = firebase.database();
export const auth = firebase.auth();
